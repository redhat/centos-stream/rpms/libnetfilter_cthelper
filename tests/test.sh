libnetfilter_cthelper_tests()
{
    local fail=0

    # add helper object
    nfct add helper ftp inet tcp || let fail++
    nfct add helper rpc inet tcp || let fail++
    nfct add helper rpc inet udp || let fail++
    nfct add helper amanda inet udp || let fail++
    nfct add helper sane inet tcp || let fail++
    nfct add helper dhcpv6 inet udp || let fail++
    nfct add helper slp inet udp || let fail++
    nfct add helper ssdp inet tcp || let fail++
    nfct add helper ssdp inet udp || let fail++
    nfct add helper tftp inet udp || let fail++
    nfct add helper tns inet tcp || let fail++
    nfct add helper mdns inet udp || let fail++

    # list helpers
    nfct list helper || let fail++

    # clear helpers
    nfct flush helper || let fail++

    # add helper object
    nfct add helper ftp inet tcp || let fail++
    nfct add helper rpc inet tcp || let fail++
    nfct add helper rpc inet udp || let fail++

    # get helper object
    nfct get helper ftp || let fail++
    nfct get helper rpc || let fail++

    # delete helper object
    nfct delete helper ftp || let fail++
    nfct delete helper rpc || let fail++

    # add helper object
    nfct add helper ftp inet tcp || let fail++
    nfct add helper rpc inet tcp || let fail++
    nfct add helper rpc inet udp || let fail++

    # get helper object
    nfct get helper ftp inet tcp || let fail++
    nfct get helper rpc inet tcp || let fail++
    nfct get helper rpc inet udp || let fail++

    # delete helper object
    nfct delete helper ftp inet tcp || let fail++
    nfct delete helper rpc inet tcp || let fail++
    nfct delete helper rpc inet udp || let fail++
    nfct delete helper amanda inet udp || let fail++
    nfct delete helper sane inet tcp || let fail++
    nfct delete helper dhcpv6 inet udp || let fail++
    nfct delete helper ssdp inet udp || let fail++
    nfct delete helper tftp inet udp || let fail++
    nfct delete helper tns inet tcp || let fail++
    nfct delete helper mdns inet udp || let fail++

    echo "There are $fail command(s) failed"
    return $fail
}

libnetfilter_cthelper_tests
